# MGMT Notes

## Description

I need to be better about the capture, collection, curation, and contemplation of information that may be valuable to my future self. The intent here was to work on synthesizing knowledge as I go from my time as an manager and other roles. 

## Setup

This project uses [mdBook](https://rust-lang.github.io/mdBook/index.html) to process Markdown files into a static site. It's used frequently by the Rust community for documentation. 

1. You'll need to have Rust installed. Follow [the instructions for installing Rust](https://www.rust-lang.org/tools/install).
2. Install the mdBook crate with `cargo install mdbook`.
3. Run `mdbook watch` to have the site be re-rendered with your changes.

## GitLab Pages

See [.gitlab-ci.yml](.gitlab-ci.yml) for build and deploy pipeline configuration details. 

With each commit to the `main` branch, a pipeline will be triggered to build the rendered site into a public artifact directory which is picked up by GitLab pages. 